package handdling

import (
	"UntypicalTest/storage"
	"encoding/json"
	"net/http"
	"strconv"
)

func HandlerCrud(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	key := r.URL.Query().Get("key")
	if len(key) == 0 {
		http.Error(w, "Key must be set", http.StatusBadRequest)
		return
	}
	var response interface{}
	switch r.Method {
	case "GET":
		var err error
		response, err = storage.Instance().Get(key)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
		}
	case "PUT":
		value := r.URL.Query().Get("value")
		if len(value) == 0 {
			http.Error(w, "value must be set", http.StatusBadRequest)
			return
		}
		response = storage.Instance().Upsert(key, value)
	case "DELETE":
		w.WriteHeader(http.StatusNoContent)
		storage.Instance().Delete(key)
	}
	err := json.NewEncoder(w).Encode(response)
	if err != nil {
		http.Error(w, "Can't encode response", http.StatusInternalServerError)
	}
}

func HandleList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	limit, _ := strconv.Atoi(r.URL.Query().Get("limit"))
	if limit < 0 || limit > storage.MaxLimit {
		http.Error(w, "Value limit is out of range", http.StatusBadRequest)
		return
	} else if limit == 0 {
		limit = storage.DefaultLimit
	}

	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	if page < 0 || page > storage.Instance().PagesCount(limit) {
		http.Error(w, "Value page is out of range", http.StatusBadRequest)
		return
	} else if page == 0 {
		page = 1
	}

	paging := Paginator{
		CurrentPage:    page,
		RecordsPerPage: limit,
		CountPages:     storage.Instance().PagesCount(limit),
		CountRecords:   storage.Instance().CountRecords(),
	}

	listData := ListResponse{
		Paginator: paging,
		Data:      storage.Instance().List(limit, page),
	}

	err := json.NewEncoder(w).Encode(listData)
	if err != nil {
		http.Error(w, "Can't write response", http.StatusInternalServerError)
	}
}
