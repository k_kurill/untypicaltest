package handdling

type Paginator struct {
	CountRecords   int `json:"countRecords"`
	CurrentPage    int `json:"currentPage"`
	CountPages     int `json:"countPages"`
	RecordsPerPage int `json:"-"`
}

type ListResponse struct {
	Data      map[string]string `json:"data"`
	Paginator Paginator         `json:"pagination"`
}
