package storage

import "sync"

const DefaultLimit = 10
const MaxLimit = 150

var storageInstance Storager

type Paging struct {
	Page       int
	CountPages int
	PerPage    int
}

type Storager interface {
	init()
	Get(key string) (string, error)
	List(perPage int, page int) map[string]string
	Upsert(key string, value string) string
	Delete(key string)
	PagesCount(perPage int) int
	CountRecords() int
}

func Instance() Storager {
	var once sync.Once
	if storageInstance == nil {
		storageInstance = new(softStorage)
		once.Do(func() {
			storageInstance.init()
		})
	}
	return storageInstance
}
