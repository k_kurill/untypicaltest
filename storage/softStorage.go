package storage

import (
	"errors"
	"sync"
)

type softStorage struct {
	data map[string]string
	sync.RWMutex
}

func (s *softStorage) init() {
	s.data = make(map[string]string)
}

func (s *softStorage) Get(key string) (string, error) {
	s.RLock()
	defer s.RUnlock()
	if value, inMap := s.data[key]; inMap {
		return value, nil
	}
	return "", errors.New("key doesn't set")
}

func (s *softStorage) List(perPage int, page int) map[string]string {
	result := make(map[string]string, perPage)
	page--
	i := 1
	for key, value := range s.data {
		if i > page*perPage && i <= (page+1)*perPage {
			result[key] = value
		}
		i++
	}
	return result
}

func (s *softStorage) Upsert(key string, value string) string {
	s.Lock()
	defer s.Unlock()
	s.data[key] = value
	return s.data[key]
}

func (s *softStorage) Delete(key string) {
	s.Lock()
	defer s.Unlock()
	delete(s.data, key)
}

func (s *softStorage) PagesCount(perPage int) int {
	s.RLock()
	defer s.RUnlock()
	count := len(s.data) / perPage
	if count == 0 {
		count = 1
	}
	return count
}

func (s *softStorage) CountRecords() int {
	s.RLock()
	defer s.RUnlock()
	return len(s.data)
}
