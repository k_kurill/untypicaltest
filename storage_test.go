package main

import (
	"UntypicalTest/handdling"
	"UntypicalTest/storage"
	"fmt"
	"net/http/httptest"
	"reflect"
	"strconv"
	"sync"
	"testing"
)

const serverUrl = "http://127.0.0.1:8081"

func TestUpsert(t *testing.T) {
	t.Run("insert records", insert)
	t.Run("udate records", update)
}

func TestDelete(t *testing.T) {
	countToDelete := 1000
	countToFill := 5000
	controlSum := fillData(countToFill, func(i int) int64 {
		return int64(i + 1)
	})

	wg := new(sync.WaitGroup)
	for i := 0; i < countToDelete; i++ {
		controlSum = controlSum - int64(i+1)
		wg.Add(1)
		go func(wg *sync.WaitGroup, i int) {
			defer wg.Done()
			r := httptest.NewRequest("DELETE", fmt.Sprintf("%s/?key=%d", serverUrl, i), nil)
			w := httptest.NewRecorder()
			handdling.HandlerCrud(w, r)
		}(wg, i)
	}
	wg.Wait()

	realSum := storageSum()

	if realSum != controlSum {
		t.Errorf("Sum elements is %d expected %d", realSum, controlSum)
	}
}

func storageSum() int64 {
	var realSum int64
	fv := reflect.ValueOf(storage.Instance()).Elem().FieldByName("data")
	iter := fv.MapRange()
	for iter.Next() {
		val, _ := strconv.Atoi(iter.Value().String())
		realSum += int64(val)
	}
	return realSum
}

func fillData(count int, indexModifier func(int) int64) int64 {
	var sum int64
	wg := new(sync.WaitGroup)
	for i := 0; i < count; i++ {
		sum += indexModifier(i)
		wg.Add(1)
		go func(wg *sync.WaitGroup, i int) {
			defer wg.Done()
			r := httptest.NewRequest("PUT", fmt.Sprintf("%s/?key=%d&value=%d", serverUrl, i, indexModifier(i)), nil)
			w := httptest.NewRecorder()
			handdling.HandlerCrud(w, r)
		}(wg, i)
	}
	wg.Wait()
	return sum
}

func insert(t *testing.T) {
	count := 5000
	controlSum := fillData(count, func(i int) int64 {
		return int64(i + 1)
	})
	if storage.Instance().CountRecords() != count {
		t.Errorf("Count records is %d expected %d", storage.Instance().CountRecords(), count)
	}

	realSum := storageSum()
	if realSum != controlSum {
		t.Errorf("Sum elements is %d expected %d", realSum, controlSum)
	}
}

func update(t *testing.T) {
	count := 5000
	controlSum := fillData(count, func(i int) int64 {
		return int64(i - 1)
	})
	if storage.Instance().CountRecords() != count {
		t.Errorf("Count records is %d expected %d", storage.Instance().CountRecords(), count)
	}

	realSum := storageSum()

	if realSum != controlSum {
		t.Errorf("Sum elements is %d expected %d", realSum, controlSum)
	}
}
